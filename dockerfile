FROM alpine:3
#ENV -e
#ENV -u
#ENV -o pipefail

#Set variable
ENV SRV_PASSWORD=”mysecurepass123”
ENV CONFIG_OUTPUT_FILE=/tmp/output

#Execute first echo command
RUN ["echo", "starting config extraction ..."]

#Execute the sshpass command
RUN sshpass -p ${SRV_PASSWORD} ssh -tt -o StrictHostKeyChecking=no -o HostKeyAlgorithms=+ssh-rsa username@10.20.2.1 "hostname" > ${CONFIG_OUTPUT_FILE}.tmp

#exexcute sleep
RUN sleep 5

#Execute echo command
RUN ["echo", "finished config extraction ..."]